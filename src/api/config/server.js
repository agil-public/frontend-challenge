const fastify = require('fastify');

const PORT = 5000;
const NODE_ENV = 'development';

const app = fastify();

app.register(require('fastify-cors'));
app.register(require('fastify-helmet'));
app.register(require('../service/routes'));

async function listen() {
  try {
    const address = await app.listen(PORT);
    console.log(
      `[${NODE_ENV.toUpperCase()}] Business-API running on: ${address}`
    );
  } catch (error) {
    console.error(`API: ${error.message}.`);
    process.exit(1);
  }
}

module.exports = { app, listen };
