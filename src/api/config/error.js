class HttpError extends Error {
  constructor({ message, statusCode }) {
    super(message);
    this.statusCode = statusCode;
  }

  getError(statusCode) {
    switch (statusCode) {
      case 500:
        return 'Internal Server Error';
      case 401:
        return 'Unauthorized';
    }
  }

  toJson() {
    return {
      error: this.getError(this.statusCode),
      statusCode: this.statusCode,
      message: this.message,
    };
  }
}

module.exports = { HttpError };
