const { readFile } = require('fs/promises');
const { resolve } = require('path');
const { HttpError } = require('../config/error');

const CONTRACTS_FILE_PATH = resolve('./src/api/data/contracts.json');
const PAYMENTS_FILE_PATH = resolve('./src/api/data/payments.json');
const STATISTICS_FILE_PATH = resolve('./src/api/data/statistics.json');

const DEFAULT_TOKEN =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IkFkbWluIiwicm9sZSI6Im1hc3RlciIsImlhdCI6MTUxNjIzOTAyMn0.aOu2kBPhX99IZGje-zZfDZORUTyf2oYsoY2IDa78OwQ';

const LOGIN_EMAIL = 'admin@agil.net';
const LOGIN_PASSWORD = '1234';

async function login(data) {
  if (data.email !== LOGIN_EMAIL || data.password !== LOGIN_PASSWORD)
    throw new HttpError({
      message: 'E-mail e/ou senha inválidos.',
      statusCode: 401,
    });

  return DEFAULT_TOKEN;
}

function authHook(request, reply, done) {
  try {
    const authorization = request.headers['authorization'];

    if (!authorization)
      throw new HttpError({
        message: 'Cabeçalho "Authorization" não encontrado na requisição.',
        statusCode: 401,
      });

    const token = authorization.split('Bearer ')[1];

    if (!token)
      throw new HttpError({
        message:
          'JWT Token não encontrado no cabeçalho "Authorization" da requisição.',
        statusCode: 401,
      });

    if (token !== DEFAULT_TOKEN)
      throw new HttpError({
        message: `JWT Token inválido: "${token}".`,
        statusCode: 401,
      });

    done();
  } catch (error) {
    const httpError =
      error instanceof HttpError
        ? error
        : new HttpError({
            message:
              error instanceof Error ? error.message : 'erro desconhecido',
            statusCode: 500,
          });
    reply
      .status(httpError.statusCode)
      .type('application/json')
      .send(httpError.toJson());
  }
}

async function findContracts() {
  const data = await readFile(CONTRACTS_FILE_PATH, 'utf-8');
  return JSON.parse(data);
}

async function findPayments() {
  const data = await readFile(PAYMENTS_FILE_PATH, 'utf-8');
  return JSON.parse(data);
}

async function findStatistics() {
  const data = await readFile(STATISTICS_FILE_PATH, 'utf-8');
  return JSON.parse(data);
}

module.exports = {
  findContracts,
  findPayments,
  findStatistics,
  authHook,
  login,
};
