const plugin = require('fastify-plugin');
const { HttpError } = require('../config/error');
const {
  findContracts,
  findPayments,
  findStatistics,
  authHook,
  login,
} = require('./main');
const { postLoginSchema } = require('./schemas');

module.exports = plugin((fast, opts, done) => {
  fast.post(
    '/login',
    {
      schema: postLoginSchema,
    },
    async (request, reply) => {
      try {
        const token = await login(request.body);

        reply.type('application/json').status(200);
        return { token };
      } catch (error) {
        const httpError =
          error instanceof HttpError
            ? error
            : new HttpError({
                message:
                  error instanceof Error ? error.message : 'erro desconhecido',
                statusCode: 500,
              });
        reply.status(httpError.statusCode).type('application/json');
        return httpError.toJson();
      }
    }
  );

  fast.get(
    '/contracts.list',
    {
      onRequest: authHook,
    },
    async (request, reply) => {
      try {
        const contracts = await findContracts();

        reply.type('application/json').status(200);
        return contracts;
      } catch (error) {
        const httpError =
          error instanceof HttpError
            ? error
            : new HttpError({
                message:
                  error instanceof Error ? error.message : 'erro desconhecido',
                statusCode: 500,
              });
        reply.status(httpError.statusCode).type('application/json');
        return httpError.toJson();
      }
    }
  );

  fast.get(
    '/payments.list',
    {
      onRequest: authHook,
    },
    async (request, reply) => {
      try {
        const payments = await findPayments();

        reply.type('application/json').status(200);
        return payments;
      } catch (error) {
        const httpError =
          error instanceof HttpError
            ? error
            : new HttpError({
                message:
                  error instanceof Error ? error.message : 'erro desconhecido',
                statusCode: 500,
              });
        reply.status(httpError.statusCode).type('application/json');
        return httpError.toJson();
      }
    }
  );

  fast.get(
    '/customers.statistics',
    {
      onRequest: authHook,
    },
    async (request, reply) => {
      try {
        const statistics = await findStatistics();

        reply.type('application/json').status(200);
        return statistics;
      } catch (error) {
        const httpError =
          error instanceof HttpError
            ? error
            : new HttpError({
                message:
                  error instanceof Error ? error.message : 'erro desconhecido',
                statusCode: 500,
              });
        reply.status(httpError.statusCode).type('application/json');
        return httpError.toJson();
      }
    }
  );

  done();
});
