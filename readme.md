# Desafio Frontend

Bem-vindo/Bem-vinda ao desafio frontend da agil.net!

![](/docs/welcome.gif)

O desafio contempla um painel administrativo responsável por exibir métricas dos serviços SAAS da agil.net, deve-se atentar a cálculos e previsões, pois seu uso será útil para tomadas de decisões importantes no que diz respeito a empresa, clientes e serviços.

## Metric SAAS Panel

### Introdução

No repositório conterá um painel de exemplo que poderá ser exibido por `./src/app/index.html`, ele é um rascunho sem estilização que demonstra o básico que esperamos que contenha no painel:

- Uma área para métricas do financeiro da empresa;
- Uma datatable com as estatísticas dos clientes, que pode ser paginada e filtrada por sua barra de pesquisa.

Você pode tanto utilizar esse painel de exemplo como base, quanto criar um do zero, nos surpreenda!

Queremos que você faça a interface visual e busque os dados na API NodeJs que está localizada em `./src/api`, mais detalhes sobre sua documentação e como colocá-la no ar podem ser encontrados [aqui](./docs/api/main.md).

### Métricas Financeiro

Esse vídeo esclaresce o funcionamento das métricas: https://youtu.be/qn9oKllrPkI.

#### Métricas independente de período

Essas métricas são gerais e os valores devem ser considerados desde o início até o fim dos dados.

- Total de contratos;
  - Dados da listagem de contratos.
- MRR, receita recorrente mensal, quanto os clientes estão pagando por mês;
  - Dados da listagem de contratos.
- ARR, receita recorrente anual, quanto a empresa gera de ganho bruto por ano;
  - Dados da listagem de contratos.
- MV, média de tempo que um cliente fica na agil.net antes de cancelar o contrato;
  - Dados da listagem de contratos.
- LTV, valor em média que o cliente deixa na agil.net da contratação até um cancelamento;
  - Dados da listagem de contratos.
- FP, percentual de uso das formas de pagamento disponibilizadas pela agil.net: cartão, boleto ou pix.
  - Dados da listagem de cobranças.

#### Métricas dependente de período

É preciso selecionar um período de abrangência dos dados para essas métricas, seja ele mensal, trimestral, semestral ou anual. Por exemplo, quero ver como foi da data 2022/01/01 até 2022/01/31.

- CAC, custo de aquisição de clientes, quanto a agil.net gasta para conquistar um único cliente;
  - Depende de um dado inserido pelo operador que é o valor investido em marketing nesse período selecionado, dados da listagem de contratos;
- CHURN, percentual de cancelamento em relação ao total de clientes na carteira.
  - Dados da listagem de contratos.

### Estatísticas dos Clientes

Uma grade onde cada linha é um cliente e cada coluna é uma informação. Deve ser possível organizar a grade de acordo com a ordem crescente/decrescente de cada coluna e pesquisar por valores na barra de pesquisa.

Dados provenientes da listagem das estatísticas dos clientes.

Colunas:

- Cliente, nome do cliente;
- Contrato, valor do contrato;
- Data de aquisição;
- Forma de pagamento: boleto, pix ou cartão;
- Usuários, total de usuários;
- Último recebimento de lead, data no formato: DD/MM/YYYY.;
- Último cadastro de imóvel, data no formato: DD/MM/YYYY.;
- Clientes, total de clientes;
- Atendimentos, total de atendimentos;
- Imóveis, total de imóveis ativos;
- Imóveis desatualizados, total de imóveis desatualizados;
- Imóveis inativos, total de imóveis inativos;
- Imagens, total de imagens;
- Condomínios, total de condomínios;
- Portais, informar se tem integração e quais portais separados por vírgula;
- Notas promissórias, total de notas promissórias.

## Requisitos Técnicos

### Obrigatórios

- Código em inglês
- Usar TypeScript
- Usar Vue.Js 3
- HTML5 e CSS3
- Disponibilizar documentação suficiente para a execução do projeto no README
- Consumo de dados da API em NodeJs

### Diferenciais

- Testes unitários
- Utilizar Figma para a prototipagem das telas
- Documentar as decisões tomadas durante o desafio técnico
- Responsividade
- Acessibilidade
- Tailwind CSS

## Avaliação

O objetivo principal deste desafio é avaliar as capacidades do candidato em:

- Uso correto e apropriado das camadas de arquitetura do projeto
- Escrever código limpo
- Estruturação e uso correto das tags HTML
- Capacidade em prototipar interfaces que atendam aos requisitos do projeto

## Orientações

Envie o link do seu repositório do desafio no GitHub para `cto@agil.net`, assim que for recebido você será notificado e daremos feedbacks conforme as avaliações que forem feitas.
