# Business-API

API responsável por administrar os dados do financeiro e dos serviços dos clientes da agil.net.

## Como subir a API

1. Instale o NodeJs v16.13.2;
2. Vá para o diretório root do projeto;
3. Rode no terminal: `npm i` para instalar as dependências;
4. Rode no terminal: `npm run api` para iniciar a API, ela escutará na porta `5000`, então a mantenha livre.

## Autenticação

Para toda requisição que não seja a de login você terá que estar autenticado para poder requisitá-la, para isso usaremos autenticação via JWT Token, mais detalhes em: https://jwt.io.

Usuário para login:

- E-mail: `admin@agil.net`
- Senha: `1234`

Requisição para login:

```
POST /login

HEADERS

{
  "Content-Type": "application/json"
}

BODY SCHEMA

{
  "email": string -- e-mail do usuário,
  "password": string -- senha do usuário
}

BODY EXAMPLE

{
  "email": "admin@agil.net",
  "password": "1234"
}

SUCCESS RESPONSE

{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IkFkbWluIiwicm9sZSI6Im1hc3RlciIsImlhdCI6MTUxNjIzOTAyMn0.aOu2kBPhX99IZGje-zZfDZORUTyf2oYsoY2IDa78OwQ"
}

ERROR RESPONSE

{
  "error": "Internal Server Error",
  "statusCode": 500,
  "message": "Unexpected error occurred."
}

```

## Rotas

### Listagem de contratos

```
GET /contracts.list

HEADERS

{
  "Authorization": "Bearer token"
}

SUCCESS RESPONSE SCHEMA

[
  {
    "id": number -- id do contrato,
    "customerName": string -- nome do cliente,
    "customerId": number -- id do cliente,
    "paymentFrequency": weekly | biweekly | monthly | quarterly | semiannually | yearly -- frequência de pagamento do contrato,
    "paymentMethod": credit_card | billet | null -- forma de pagamento do contrato, se nulo o cliente pode escolher entre as formas de pagamento disponíveis,
    "paymentRepeats": number | null -- número de quantas cobranças serão geradas para o contrato, se nulo é até cancelar,
    "nextDueDate": string | null -- próxima data de vencimento do contrato, se nulo o contrato ainda não está ativo,
    "fulfilled": boolean -- se o contrato já atingiu ou não o número de cobranças geradas,
    "totalValue": number -- valor total do contrato,
    "createdAt": string -- data de criação do contrato,
    "updatedAt": string | null -- data de atualização do contrato, se nulo o contrato não foi atualizado,
    "deletedAt": string | null -- data de remoção do contrato, se nulo o contrato não foi removido
  }
]

SUCCESS RESPONSE EXAMPLE

[
  {
    "id": 1594,
    "customerName": "Joãozinho",
    "customerId": 1926,
    "paymentFrequency": "monthly",
    "paymentMethod": null,
    "paymentRepeats": null,
    "nextDueDate": "2022-03-26T00:00:00.000Z",
    "fulfilled": false,
    "totalValue": 299,
    "createdAt": "2022-01-21T17:00:47.292Z",
    "updatedAt": "2022-02-21T00:47:14.314Z",
    "deletedAt": null
  }
]

ERROR RESPONSE

{
  "error": "Internal Server Error",
  "statusCode": 500,
  "message": "Unexpected error occurred."
}
```

### Listagem de cobranças

```
GET /payments.list

HEADERS

{
  "Authorization": "Bearer token"
}

SUCCESS RESPONSE SCHEMA

[
  {
    "id": number -- id da cobrança,
    "contractId": number | null -- id do contrato, se não houver é uma cobrança avulsa, se tiver é cobrança de um contrato,
    "customerId": number -- id do cliente,
    "customerName": string -- nome do cliente,
    "value": number -- valor da cobrança, se vencida estará acrescido de multa/juros,
    "originalValue": number -- valor original da cobrança, sem encargos,
    "netValue": number -- valor líquido da cobrança, descontada a taxa da forma de pagamento,
    "fineValue": number -- porcentagem de multa,
    "interestValue": number -- porcentagem de juros ao mês,
    "dueDate": string -- data de vencimento da cobrança,
    "originalDueDate": string -- data de vencimento original da cobrança,
    "paidAt": string | null -- data de confirmação de pagamento da cobrança, se nulo a cobrança não foi paga,
    "paymentMethod": billet | credit_card | debit_card | pix | null -- forma de pagamento da cobrança, se nulo a forma de pagamento ainda não foi definida,
    "receivedAt": string | null -- data de recebimento em conta da cobrança, se nulo a cobrança não foi recebida em conta,
    "status": overdue | pending | confirmed | received | received_in_cash -- status da cobrança,
    "createdAt": string -- data de criação da cobrança,
    "updatedAt": string | null -- data de atualização da cobrança, se nulo a cobrança não foi atualizada,
    "deletedAt": string | null -- data de remoção da cobrança, se nulo a cobrança não foi removida
  }
]


SUCCESS RESPONSE EXAMPLE

[
  {
    "id": 4369,
    "contractId": null,
    "customerId": 1766,
    "customerName": "Joãozinho",
    "value": 195,
    "originalValue": 195,
    "netValue": 192.01,
    "fineValue": 5,
    "interestValue": 0.9,
    "dueDate": "2021-07-10T00:00:00.000Z",
    "originalDueDate": "2021-07-10T00:00:00.000Z",
    "paidAt": "2021-07-08T00:00:00.000Z",
    "paymentMethod": "billet",
    "receivedAt": "2021-07-09T00:00:00.000Z",
    "status": "received",
    "createdAt": "2021-06-01T10:17:17.000Z",
    "updatedAt": null,
    "deletedAt": null
  }
]

ERROR RESPONSE

{
  "error": "Internal Server Error",
  "statusCode": 500,
  "message": "Unexpected error occurred."
}
```

### Listagem das estatísticas dos clientes

```
GET /customers.statistics

HEADERS

{
  "Authorization": "Bearer token"
}

SUCCESS RESPONSE SCHEMA

[
  {
    "customerName": string -- nome do cliente,
    "contract": {
      "totalValue": number -- valor total do contrato,
      "createdAt": string -- data de aquisição do contrato,
      "paymentMethod": credit_card | billet | null -- forma de pagamento do contrato, se nulo o cliente pode escolher entre as formas de pagamento disponíveis
    },
    "users": number -- total de usuários,
    "lastLead": string -- data do último recebimento de lead,
    "lastProperty": string -- data do último cadastro de imóvel,
    "customers": number -- total de clientes,
    "calls": number -- total de atendimentos,
    "properties": number -- total de imóveis ativos,
    "outdatedProperties": number -- total de imóveis desatualizados,
    "inactiveProperties": number -- total de imóveis inativos,
    "images": number -- total de imagens,
    "condominiums": number -- total de condomínios,
    "portals": array<string> -- lista de nomes de portais em uso, pode ser vazia,
    "notes": number -- total de notas promissórias
  }
]


SUCCESS RESPONSE EXAMPLE

[
 {
    "customerName": "Pedrinho",
    "contract": {
      "totalValue": 350,
      "createdAt": "2021-06-01T10:17:17.000Z",
      "paymentMethod": "billet"
    },
    "users": 200,
    "lastLead": "2021-06-01T10:17:17.000Z",
    "lastProperty": "2021-06-01T10:17:17.000Z",
    "customers": 500,
    "calls": 10000,
    "properties": 2000,
    "outdatedProperties": 5000,
    "inactiveProperties": 6000,
    "images": 50000,
    "condominiums": 1000,
    "portals": ["zap", "viva_real"],
    "notes": 20000
  }
]

ERROR RESPONSE

{
  "error": "Internal Server Error",
  "statusCode": 500,
  "message": "Unexpected error occurred."
}
```
